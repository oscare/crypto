#!/usr/bin/python3
import fileinput

# Permutacion para la llave ENCRYPT
tableu = [['E','N','C','R','Y'],
          ['P','T','A','B','D'],
          ['F','G','H','I','K'],
          ['L','M','O','Q','S'],
          ['U','V','W','X','Z']
         ]

def get_args():
    args = []
    for line in fileinput.input():
        args.append(line.strip())
    return args

def get_indexes(ch):
    for row in range(len(tableu)):
        for column in range(len(tableu[row])):
            if tableu[row][column] == ch:
                return (row, column)

def get_chiperM(new_indexes):
    cryptoM = ""
    while new_indexes:
        r = new_indexes.pop(0)
        c = new_indexes.pop(0)
        cryptoM += tableu[r][c]

    return cryptoM

def bifid_encrypt(clearm):
    """
        Funcion para realizar el cifrado usando
        el algoritmo Bifid

        recibe -> clearm    String sin espacios
        retorna -> String de criptomensaje
    """
    rows = []
    columns = []
    for ch in clearm:
        r, c = get_indexes(ch)
        rows.append(r)
        columns.append(c)
    rows.extend(columns)
    return get_chiperM(rows)


def get_clearM(cryptoM):
    clear = ""
    j = int(len(cryptoM)/2)
    for i in range(int(len(cryptoM)/2)):
        c = tableu[cryptoM[i]][cryptoM[j]]
        clear += c
        j += 1
    return clear

def bifid_decrypt(cryptoM):
    indexes = []
    for ch in cryptoM:
        r, c = get_indexes(ch)
        indexes.append(r)
        indexes.append(c)
    return get_clearM(indexes)

def main():
    args = get_args()
    if args[0].upper() == "ENCRYPT":
        print(bifid_encrypt(args[1].replace(" ","")))
    elif args[0].upper() == "DECRYPT":
        print(bifid_decrypt(args[1].replace(" ","")))

if __name__ == '__main__':
    main()
