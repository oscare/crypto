#!/usr/bin/python3
import fileinput

def get_args():
    args = []
    for line in fileinput.input():
        args.append(line.strip())
    return args

def main():
    args = get_args()
    a = bytearray(args[1], 'utf-8')
    print(a)
    print(str(a[8]))

if __name__ == "__main__":
    main()
