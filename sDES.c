#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int initial[] = {1, 5, 2, 0, 3, 7, 4, 6},
    initial_i[] = {3, 0, 2, 4, 6, 1, 7, 5},
    subkr1[] = {2, 4, 1, 6, 3, 9, 0, 8, 7, 5},
    subkr2[] = {5, 2, 6, 3, 7, 4, 9, 8},
    expansion[] = {3, 0, 1, 2, 1, 2, 3, 0},
    permute_halves[] = {1, 3, 2, 0},
    S0 [][4]= {{1, 0, 3, 2},
	    {3, 2, 1, 0},
	    {0, 2, 1, 3},
	    {3, 1, 3, 2} },
    S1 [][4] = { {0, 1, 2, 3},
	    {2, 0, 1, 3},
	    {3, 0, 1, 0},
	    {2, 1, 0, 3} };

int read_args(char * funct, char * key, char * plaint){
	char a;
	scanf("%1c%*c", funct);
	scanf("%10c", key);
	//to ommit \n
	scanf("%c", &a);
	scanf("%8c", plaint);
	return 0;
}

int permutation(char *array, int *perm_array){
	int i;
	// temporary array to do permutation over plaint
	char *tmp = (char *)calloc(sizeof(array), sizeof(char));
	strcpy(tmp, array);
	for (i=0; i< sizeof(array); i++){ 
		array[i] = tmp[perm_array[i]];
	}
	return 0;
}

int circular_shift(char *arr, int lenght, int rounds){
	int i, j;
	char tmp;
	for (j=0; j<rounds; j++){
		tmp = arr[0];
		for (i=0; i<lenght; i++){
			arr[i] = arr[i+1];
		}
		arr[lenght-1] = tmp;
	}
	return 0;
}

int get_subk(char *sk, char *half1, char *half2, int *positions){
	int i;
	for (i=0; i<sizeof(positions); i++){
		if (positions[i] < 5)
			sk[i] = half1[positions[i]];
		else
			sk[i] = half2[positions[i] - 5];

	}
	return 0;
}

int subkeys(char *k, char *k1, char *k2){ 
	char *half1 = (char *)calloc(5, sizeof(char)),
	     *half2 = (char *)calloc(5, sizeof(char));
	permutation(k, subkr1);
	//get the left half
	strncpy(half1, k, 5);
	//get the right half
	strncpy(half2, k + 5, 5);
	circular_shift(half1, 5, 1);
	circular_shift(half2, 5, 1);
	get_subk(k1, half1, half2, subkr2);
	circular_shift(half1, 5, 2);
	circular_shift(half2, 5, 2);
	get_subk(k2, half1, half2, subkr2);

	return 0;
}

int expande(char *halve, char *expanded, int *expansion){ 
	int i;
	for (i=0; i<8; i++){
		expanded[i] = halve[expansion[i]];
	}

	return 0;
}

int xor(char *subk, char*h, int lenght){
	int i;
	for (i=0; i<lenght; i++){
		// Adding 48 to get ASCII value
		subk[i] = (char) (subk[i] ^ h[i]) + 48;
	}

	return 0;
}

int to_int(char c1, char c2){
	if(c1=='0' && c2=='0'){
		return 0;
	}
	if(c1=='0' && c2=='1'){
                return 1;
	}
	if(c1=='1' && c2=='0'){
                return 2;
	}
	if(c1=='1' && c2=='1'){
                return 3;
	}
}

int to_charArr(char *s, int Sm){
	if (Sm == 0){
		s[0] = '0';
		s[1] = '0';
	}
	if (Sm == 1){
                s[0] = '0';
                s[1] = '1';
        }
	if (Sm == 2){
                s[0] = '1';
                s[1] = '0';
        }
	if (Sm == 3){
                s[0] = '1';
                s[1] = '1';
        }

	return 0;
}

char * mixing_func(char *h1, char *h2, char *subk){
	char *expanded = (char *)calloc(8, sizeof(char)),
	     *s0 = (char *)calloc(2, sizeof(char)),
	     *s1 = (char *)calloc(2, sizeof(char)),
	     *permuted = (char *)calloc(4, sizeof(char));
		
	expande(h2, expanded, expansion);
	xor(subk,expanded, 8);
	//strncpy(h1, subk, 4);
	//strncpy(h2, subk + 4, 4);
	

	to_charArr(s0, S0[to_int(subk[0],subk[3])][to_int(subk[1],subk[2])]);
	to_charArr(s1, S1[to_int(subk[4],subk[7])][to_int(subk[5],subk[6])]);
	strncpy(permuted, s0, 2);
	strncpy(permuted + 2, s1, 2);
	permutation(permuted, permute_halves);
	return permuted;
	
}
int feistel(char *plaint, char *subk, int step){
	char *halve1 = (char *)calloc(4, sizeof(char)),
	     *halve2 = (char *)calloc(4, sizeof(char)),
	     *permuted = (char *)calloc(4, sizeof(char));

	strncpy(halve1, plaint, 4);
	strncpy(halve2, plaint + 4, 4);
	permuted = (char *) mixing_func(halve1, halve2, subk);
	xor(halve1, permuted, 4);

	//concatenating and interchanging halves on plaint
	if (step == 1){
		strncpy(plaint, halve2, 4);
		strncpy(plaint + 4, halve1, 4);
	}
	//concatenating in order
	else{
		strncpy(plaint, halve1, 4);
		strncpy(plaint + 4, halve2, 4);
	}

}

int encrypt(char *key, char *plaint){

	char * k1 = (char*)calloc(8, sizeof(char)),
             * k2 = (char *)calloc(8, sizeof(char));

	permutation(plaint, initial);
	subkeys(key, k1, k2);
	feistel(plaint, k1, 1);
	feistel(plaint, k2, 2);
	permutation(plaint, initial_i);
	printf("%s", plaint);

	return 0;
}

int decrypt(char * key, char * cipherm) {

	char * k1 = (char*)calloc(8, sizeof(char)),
             * k2 = (char *)calloc(8, sizeof(char));
	
	permutation(cipherm, initial);
	subkeys(key, k1, k2);
	feistel(cipherm, k2, 1);
	feistel(cipherm, k1, 2);
	permutation(cipherm, initial_i);
	printf("%s", cipherm);

	return 0;
}
int main(){
	//allocate mem with zeros
	char * funct = (char*)calloc(1, sizeof(char)),
	     * plaint = (char*)calloc(8, sizeof(char)),
	     * key = (char*)calloc(10, sizeof(char));

	read_args(funct, key, plaint);

	*funct == 'E' ? encrypt(key, plaint) : *funct == 'D' ? decrypt(key, plaint) : printf("\nBad request\n");

	return 0;
}
